<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <title>Maresias - Calculadora</title>

        <!-- >> CSS
        ============================================================================== -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Google Web Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Raleway:500,300,300italic,500italic,700,700italic' rel='stylesheet' type='text/css'>
        <!-- Font Awesome -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- animate.css -->
        <link href="vendor/animate.css" rel="stylesheet">
        <!-- fancybox.css -->
        <link href="vendor/fancybox/jquery.fancybox.css" rel="stylesheet">
        <!-- owl carousel -->
        <link href="vendor/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
        <!-- slideControl -->
        <link href="vendor/slidecontrol/slideControl.css" rel="stylesheet">
        <!-- Main Styles -->
        <link href="css/styles.css" rel="stylesheet">

        <!-- >> /CSS
        ============================================================================== -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="vendor/jquery.min.js"></script>
        <script src="vendor/jquery.maskMoney.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <!-- slideControl -->
        <script src="vendor/slidecontrol/jquery.slideControl.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('.slideControl').slideControl();
                $('.slideControl2').slideControl2();
            });
        </script>

        <style>
            .input-error +label:before{
                border: 1px solid #ff0000 !important;
            }
        </style>



    </head>
    <body>

        <!-- ABRE MODAL: Tratamento -->
        <div class="modal modal-tratamento" id="mtratamento" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body body-tratamento">
                                <div class="title text-center">Tratamento</div>
                                <br>
                                <div class="col-md-5 col-md-offset-1">
                                    <div class="btn abre-calc">Calculadora de Piscinas</div>
                                </div>
                                <div class="col-md-5">
                                    <div class="btn abre-calc2">Calculadora de SPAS</div>
                                </div>
                            </div>
                        </div>

                        <div class="calculadora text-left">
                            <div class="calc-1">
                                <div class="col-md-12">
                                    <div class="title">Etapa 1 - Equilíbrio Químico</div>
                                    <div class="col-md-12">
                                        <p>Consiste em equilibrar os níveis de pH e Alcalinidade e mantê-los na faixa ideal para que os produtos possam agir de forma eficaz.</p>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <li><input type="text" value="" class="form-control calc1-volume required" placeholder="Volume da Água da Piscina (litros)" data-id="input-volume"></li>
                                        <div class="pequeno text-right abre-volume">não sei o volume de água da piscina</div>

                                        <li><input type="text" value="" class="slideControl form-control calc1-ph required" data-id="input-ph" placeholder="pH"/></li>
                                        <div class="pequeno text-right abre-ph">como medir o pH?</div>

                                        <li class="segundo"><input type="text" value="" class="slideControl2 form-control calc1-alc required" data-id="input-alcalinidade" placeholder="Alcalinidade Total (PPM)" /></li>
                                        <div class="pequeno text-right abre-alcalinidade">como medir a Alcalinidade Total?</div>

                                    </div>

                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="btn abre-video-1">Veja o vídeo explicativo</div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="btn proximo" data-id='calc1-result'>Ver resultado</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-1-volume">
                                <div class="col-md-12">
                                    <div class="title">Cálculo de Volume de Água da Piscina</div>
                                    <div class="col-md-12">
                                        <p>Não sabe qual o volume de água da sua piscina? Nós te ajudamos!</p>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="col-md-12"><div class="opcao-calc">Qual o formato da piscina?</div></div>
                                        <div class="col-md-3 text-center menu-piscinavolume menu-piscinavolume-1"><img src="img/piscina-retangular.png" class="responsive-image"><br>Piscina Retangular</div>
                                        <div class="col-md-3 text-center menu-piscinavolume menu-piscinavolume-2"><img src="img/piscina-redonda.png" class="responsive-image"><br>Piscina Redonda</div>
                                        <div class="col-md-3 text-center menu-piscinavolume menu-piscinavolume-3"><img src="img/piscina-feijao.png" class="responsive-image"><br>Piscina em Oito, Feijão ou Oval</div>
                                        <div class="col-md-3 text-center menu-piscinavolume menu-piscinavolume-4"><img src="img/piscina-irregular.png" class="responsive-image"><br>Piscina Irregular</div>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1 piscinavolume piscinavolume-1">
                                        <div class="col-md-12">
                                            <div class="opcao-calc">Piscina Retangular</div>
                                            <p>Comprimento x largura x profundidade média = volume em M³</p>
                                        </div>
                                        <div class="col-md-3">
                                            <li><input type="text" value="" class="form-control input-retangular retangular-comprimento" placeholder="Comprimento"></li>
                                        </div>
                                        <div class="col-md-3">
                                            <li><input type="text" value="" class="form-control input-retangular retangular-largura" placeholder="Largura"></li>
                                        </div>
                                        <div class="col-md-3">
                                            <li><input type="text" value="" class="form-control input-retangular retangular-profundidade" placeholder="Profundidade média"></li>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-control result"><span class='result-volume-retangular'></span> M³</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <p><br><br><strong>**Profundidade média:</strong> profundidade maior + profundidade menor / 2<br>
                                                <strong>**M³:</strong> Contém 1.000 litros</p>
                                        </div>
                                        <div class="col-md-12 text-center margem">
                                            <div class="btn2 proximo">Voltar à Parte 1</div>
                                        </div>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1 piscinavolume piscinavolume-2">
                                        <div class="col-md-12">
                                            <div class="opcao-calc">Piscina Redonda</div>
                                            <p>Diâmetro x profundidade média x 0,785 = volume em M³</p>
                                        </div>
                                        <div class="col-md-4">
                                            <li><input type="text" value="" class="form-control input-redonda redonda-diametro" placeholder="Diâmetro"></li>
                                        </div>
                                        <div class="col-md-4">
                                            <li><input type="text" value="" class="form-control input-redonda redonda-profundidade" placeholder="Profundidade média"></li>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-control result"><span class='result-volume-redonda'></span> M³</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <p><br><br><strong>**Profundidade média:</strong> profundidade maior + profundidade menor / 2<br>
                                                <strong>**M³:</strong> Contém 1.000 litros</p>
                                        </div>
                                        <div class="col-md-12 text-center margem">
                                            <div class="btn2 proximo">Voltar à Parte 1</div>
                                        </div>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1 piscinavolume piscinavolume-3">
                                        <div class="col-md-12">
                                            <div class="opcao-calc">Piscina em Oito, Feijão ou Oval</div>
                                            <p>Diâmetro maior x diâmetro menor x profundidade média x 0,785 = volume em M³</p>
                                        </div>
                                        <div class="col-md-3">
                                            <li><input type="text" value="" class="form-control input-oito oito-menor" placeholder="Diâmetro maior"></li>
                                        </div>
                                        <div class="col-md-3">
                                            <li><input type="text" value="" class="form-control input-oito oito-maior" placeholder="Diâmetro menor"></li>
                                        </div>
                                        <div class="col-md-3">
                                            <li><input type="text" value="" class="form-control input-oito oito-profundidade" placeholder="Profundidade média"></li>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-control result"><span class='result-volume-oito'></span> M³</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <p><br><br><strong>**Profundidade média:</strong> profundidade maior + profundidade menor / 2<br>
                                                <strong>**M³:</strong> Contém 1.000 litros</p>
                                        </div>
                                        <div class="col-md-12 text-center margem">
                                            <div class="btn2 proximo">Voltar à Parte 1</div>
                                        </div>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1 piscinavolume piscinavolume-4">
                                        <div class="col-md-12"><div class="opcao-calc">Piscina Irregular</div></div>
                                        <div class="col-md-12"><p>Divide-se o modelo em partes regulares(retângulo, circulo ou oval), calcula-se as varias áreas individuais, que deverão ser somadas para obter-se o resultado para da área total.</p></div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <p><br><strong>**Profundidade média:</strong> profundidade maior + profundidade menor / 2<br>
                                                <strong>**M³:</strong> Contém 1.000 litros</p>
                                        </div>
                                        <div class="col-md-12 text-center margem">
                                            <div class="btn2 proximo">Voltar à Parte 1</div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-1-ph">
                                <div class="col-md-12">
                                    <div class="title">Cálculo de pH</div>
                                    <div class="col-md-12">
                                        <p>Medição da quantidade de íons de Hidrogênio dispersos na água, o que na verdade, determina se a água está ácida, está neutra ou está alcalina (básica).</p>
                                    </div>


                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="opcao-calc">Como medir?</div>
                                        <p>
                                            <strong>1 -</strong> Mergulhar o tubo transparente a aproximadamente 30 cm da superfície da piscina;<br>
                                            <strong>2 -</strong> Descartar o excesso de água até que ela atinja o nível indicado;<br>
                                            <strong>3 -</strong> Colocar 6 gotas do REAGENTE DE pH, tampar o frasco e homogeneizá-lo, agitando-o circularmente por alguns segundos;<br>
                                            <strong>4 -</strong> Fazer a leitura do nível do pH, comparando a cor obtida com a escala de cores impressa no tubo. Dependendo do resultado obtido deverá ser usado Redutor ou Elevador de pH para efetuar as possíveis correções;<br>
                                            <strong>5 -</strong> Descartar o conteúdo do tubo transparente (não devolva à piscina);<br>
                                            <strong>6 -</strong> Lavar o tubo com esponja e água corrente.<br><br>

                                            Obs.: A escala de pH varia de 0 a 14; Se após realizar o procedimento de correção, o resultado colorimétrico permanecer inalterado, significa que o pH ainda está fora do intervalo ideal, sendo necessário repetir o procedimento até atingir os valores indicados;<br><br>
                                            Usar preferencialmente um fundo branco para fazer a leitura. A intensidade de luz pode resultar em uma pequena variação na escala colorimétrica da reação.
                                        </p>
                                    </div>

                                    <div class="col-md-12 text-center">
                                        <div class="btn2 proximo">Voltar à Parte 1</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-1-alcalinidade">
                                <div class="col-md-12">
                                    <div class="title">Cálculo de Alcalinidade Total</div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="opcao-calc">Como medir?</div>
                                        <p>
                                            <strong>1 -</strong> Mergulhar o tubo transparente a aproximadamente 30 cm da superfície da piscina;<br>
                                            <strong>2 -</strong> Descartar o excesso de água até que ela atinja o nível indicado;<br>
                                            <strong>3 -</strong> Colocar 03 gotas da SOLUÇÃO 01 (reagente indicador) e homogeneíze o frasco agitando-o circularmente por alguns segundos, ficará com uma coloração roxa;<br>
                                            <strong>4 -</strong> Adicionar a SOLUÇÃO 02 (reagente titulador) no tubo transparente, gota a gota, agitando-o da mesma forma. É necessário fazer contagem de quantas gotas foram adicionadas até o liquido mudar para cor a amarela;<br>
                                            <strong>5 -</strong> Multiplique o número de gotas do reagente TITULADOR por 10, o resultado desta multiplicação é o valor da Alcalinidade Total da água da piscina;<br>
                                            Exemplo 1: Se o número de gotas utilizadas até a solução atingir a cor amarela for superior a 13, ou seja, 130 ppm, significa que a Alcalinidade esta levemente alta.<br>
                                            Se for necessário abaixar a Alcalinidade Total, utilizar o redutor de pH.<br>
                                            Exemplo 2: Se ao colocar as primeiras gotas da solução, o conteúdo do frasco já mude para a cor amarela, isso indica que a Alcalinidade Total está baixa (quanto menos gotas, mais próxima a zero ppm) e deverá ser corrigida. Utilizar o Elevador de Alcalinidade Total;<br>
                                            <strong>6 -</strong> Descartar o conteúdo do frasco (não devolva à piscina);<br>
                                            <strong>7 -</strong> Para maiores informações consultar o rótulo do produto.<br><br>
                                            Obs.: Usar preferencialmente um fundo branco para fazer a leitura. A intensidade de luz pode resultar em uma pequena variação na escala colorimétrica da reação.<br><br>
                                            Entre as impurezas encontradas nas águas, existem aquelas que são capazes de reagir com ácidos, podendo neutralizar certa quantidade desses reagentes. Essas impurezas conferem às águas a característica de alcalinidade. Por definição, alcalinidade total de uma água é a sua capacidade quantitativa de neutralizar um ácido forte, até um determinado pH.

                                        </p>
                                    </div>

                                    <div class="col-md-12 text-center">
                                        <div class="btn2 proximo">Voltar à Parte 1</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-1-video">
                                <div class="col-md-12">
                                    <div class="title">Vídeo explicativo</div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="videoWrapper">
                                            <!-- Copy & Pasted from YouTube -->
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/eKFTSSKCzWA?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-center">
                                        <div class="btn2 proximo">Voltar à Parte 1</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-1-result">
                                <div class="col-md-12">
                                    <div class="title">Etapa 1 - Equilíbrio Químico</div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="resultado" data-id='ph-baixo' style="display: none;">
                                            <div class="opcao-calc">O nível de pH está baixo - Veja como corrigir:</div>
                                            <p>
                                                Se você obteve o pH menor que 7,2, utilize o ELEVADOR DE pH MARESIAS para efetuar o ajuste do pH, conforme descrito abaixo:<br><br>
                                                <strong>1 -</strong> Colocar a motobomba na posição recircular ou filtrar;<br>
                                                <strong>2 -</strong> Diluir o ELEVADOR DE pH em um recipiente plástico com um pouco de água da piscina, na dosagem recomendada (vide tabela embalagem);<br>
                                                <strong>3 -</strong> Necessário agitar para misturar bem o produto na água;<br>
                                                <strong>4 -</strong> Distribuir na superfície da piscina e longe das partes metálicas, o produto diluído e deixar a motobomba ligada durante um período de 1 a 2 horas. Não se deve usar a piscina nesse intervalo;<br>
                                                <strong>5 -</strong> Após a homogeneização, medir novamente o pH, caso não obtenha o intervalo ideal, repetir o processo;<br>
                                                <strong>6 -</strong> Cuidado ao adicionar dosagens de choque, que poderão comprometer o processo.
                                            </p>
                                            <table align="center" width="500px">
                                                <tr>
                                                    <td width="166px" data-id="td-ph"></td>
                                                    <!--<td width="166px" data-id="td-litros"></td>-->
                                                    <td data-id="td-produtos"></td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="resultado" data-id='ph-alto' style="display: none;">
                                            <div class="opcao-calc">O nível de pH está alto - Veja como corrigir:</div>
                                            <p>
                                                Se a leitura do pH resultou em um valor maior que 7,6 use o Redutor de pH Maresias para proceder com a correção, conforme descrito abaixo:<br><br>
                                                <strong>1 -</strong> Colocar a motobomba na posição recircular ou filtrar;<br>
                                                <strong>2 -</strong> Diluir o REDUTOR DE pH em um recipiente plástico com um pouco de água da piscina, na dosagem recomendada (vide tabela embalagem);<br>
                                                <strong>3 -</strong> Necessário agitar para misturar bem o produto na água;<br>
                                                <strong>4 -</strong> Distribuir na superfície da piscina e longe das partes metálicas, o produto diluído e deixar a motobomba ligada durante um período de 1 a 2 horas. Não se deve usar a piscina nesse intervalo;<br>
                                                <strong>5 -</strong> Após a homogeneização, medir novamente o pH, caso não obtenha o intervalo ideal, repetir o processo;<br>
                                                <strong>6 -</strong> Cuidado ao adicionar dosagens de choque, que poderão comprometer o processo.
                                            </p>
                                            <table align="center" width="500px">
                                                 <tr>
                                                    <td width="166px" data-id="td-ph"></td>
                                                    <td data-id="td-produtos"></td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="resultado" data-id='ph-ok' style="display: none;">
                                            <div class="opcao-calc">O nível de pH está ok - Parabéns</div>
                                        </div>

                                        <div class="resultado" data-id='alc-baixa' style="display: none;">
                                            <div class="opcao-calc">A Alcalinidade Total está baixa - Veja como corrigir:</div>
                                            <p>
                                                Você obteve a Alcalinidade Total abaixo de 80 PPM, utilize o ELEVADOR DE ALCALINIDADE MARESIAS para efetuar o ajuste, conforme descrito abaixo:<br><br>
                                                <strong>1 -</strong> Colocar a motobomba na posição recircular ou filtrar;<br>
                                                <strong>2 -</strong> Diluir o ELEVADOR DE ALCALINIDADE em um recipiente plástico com um pouco de água da piscina, na dosagem recomendada (vide tabela embalagem);<br>
                                                <strong>3 -</strong> Necessário agitar para misturar bem o produto na água;<br>
                                                <strong>4 -</strong> Distribuir na superfície da piscina e longe das partes metálicas, o produto diluído e deixar a motobomba ligada durante um período de 1 a 2 horas. Não se deve usar a piscina nesse intervalo;<br>
                                                <strong>5 -</strong> Após a homogeneização, medir novamente a Alcalinidade Total. Caso não obtenha o intervalo ideal, repetir o processo;<br>
                                                Cuidado ao adicionar dosagens de choque, que poderão comprometer o processo.<br><br>
                                                * Importante: Cada dosagem equivale aumentar 10 ppm por vez.
                                            </p>
                                            <table align="center" width="500px">
                                                <tr>
                                                    <td width="166px" data-id='td-ppm'>abaixo de 80 ppm</td>
                                                    <!--<td width="166px" data-id='td-litros'>1.000 litros</td>-->
                                                    <td data-id='td-produtos'>18 gr</td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="resultado" data-id='alc-alta' style="display: none;">
                                            <div class="opcao-calc">A Alcalinidade Total está alta - Veja como corrigir:</div>
                                            <p>
                                                Se a leitura da Alcalinidade Total resultou em um valor acima de 120 PPM, use o REDUTOR DE pH e ALCALINIDADE TOTAL MARESIAS para proceder com a correção, conforme descrito abaixo:<br><br>
                                                <strong>1 -</strong> Colocar a motobomba na posição recircular ou filtrar;<br>
                                                <strong>2 -</strong> Diluir o REDUTOR DE pH e ALCALINIDADE TOTAL MARESIAS em um recipiente plástico com um pouco de água da piscina, na dosagem recomendada (vide tabela embalagem);<br>
                                                <strong>3 -</strong> Necessário agitar para misturar bem o produto na água;<br>
                                                <strong>4 -</strong> Distribuir na superfície da piscina e longe das partes metálicas, o produto diluído e deixar a motobomba ligada durante um período de 1 a 2 horas. Não se deve usar a piscina nesse intervalo;<br>
                                                <strong>5 -</strong> Após a homogeneização, medir novamente a Alcalinidade Total. Caso não obtenha o intervalo ideal, repetir o processo;<br>
                                                Cuidado ao adicionar dosagens de choque, que poderão comprometer o processo.<br><br>
                                                * Importante: Cada dosagem equivale aumentar 10 PPM por vez.
                                            </p>
                                            
                                            <table align="center" width="500px">
                                                 <tr>
                                                    <td width="166px" data-id="td-alc"></td>
                                                    <td data-id="td-produtos"></td>
                                                </tr>
                                            </table>
                                            
                                        </div>

                                        <div class="resultado" data-id='alc-ok' style="display: none;">
                                            <div class="opcao-calc">A Alcalinidade Total está ok - Parabéns</div>
                                        </div>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="btn proximo" data-id='calc2-show'>Avançar Calculadora</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-2">
                                <div class="col-md-12">
                                    <div class="title">Etapa 2 - Sanitização</div>
                                    <div class="col-md-12"><p>Consiste em adicionar à água sanitizantes, oxidantes e algicidas que removem micro-organismos potencialmente prejudiciais à saúde.</p></div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="opcao-calc">Estado da Água:</div>

                                        <input type="radio" name="estadoagua" value="transparente" id="estadotransparente">
                                        <label for="estadotransparente"><span>Transparente</span></label>

                                        <input type="radio" name="estadoagua" value="verde" id="estadoverde">
                                        <label for="estadoverde"><span>Verde</span></label>

                                        <div class="opcao-calc">Frequência de Usuários:</div>


                                        <input type="radio" name="frequenciauso" value="publica" id="frequenciapublica">
                                        <label for="frequenciapublica"><span>Pública</span></label>

                                        <input type="radio" name="frequenciauso" value="menor" id="frequencia10">
                                        <label for="frequencia10"><span>Residencial até 10</span></label>

                                        <input type="radio" name="frequenciauso" value="maior" id="frequenciamais10">
                                        <label for="frequenciamais10"><span>Residencial 10+</span></label>


                                        <div class="opcao-calc">Tratamento Alternativo:</div>


                                        <input type="radio" name="tratamentoalternativo" value="ozonio" id="tratamentoozonio">
                                        <label for="tratamentoozonio"><span>Ozônio</span></label>

                                        <input type="radio" name="tratamentoalternativo" value="ionizador" id="tratamentoionizador">
                                        <label for="tratamentoionizador"><span>Ionizador</span></label>

                                        <input type="radio" name="tratamentoalternativo" value="uvc" id="tratamentouvc">
                                        <label for="tratamentouvc"><span>UVC</span></label>

                                        <input type="radio" name="tratamentoalternativo" value="nenhum" id="tratamentonenhum">
                                        <label for="tratamentonenhum"><span>Nenhum</span></label>


                                    </div>

                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="btn abre-video-2">Veja o vídeo explicativo</div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="btn proximo" data-id='calc2-result'>Ver resultado</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-2-video">
                                <div class="col-md-12">
                                    <div class="title">Vídeo explicativo</div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="videoWrapper">
                                            <!-- Copy & Pasted from YouTube -->
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/eKFTSSKCzWA?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-center">
                                        <div class="btn2 proximo">Voltar à Parte 2</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-2-result">
                                <div class="col-md-12">
                                    <div class="title">Etapa 2 - Sanitização</div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="resultado" data-id='publica' style="display: none;">
                                            <div class="opcao-calc">Piscina Pública</div>
                                            <p>
                                                Fazer a medição com o Mkit a cada 03 dias.
                                            </p>

                                            <table align="center" width="600px" data-id='transparente' style="display: none;">
                                                <tr>
                                                    <td width="300px">MPlus - Água transparente</td>
                                                    <!--<td width="150px" data-id='td-litros'></td>-->
                                                    <td data-id='td-produtos'></td>
                                                </tr>
                                            </table>

                                            <table align="center" width="600px" data-id='verde' style="display: none;">
                                                <tr>
                                                    <td width="300px">MPlus - Água verde</td>
                                                    <!--<td width="150px" data-id='td-litros'></td>-->
                                                    <td data-id='td-produtos'></td>
                                                </tr>
                                            </table>

                                            <table align="center" width="600px" data-id='m20'>
                                                <tr>
                                                    <td width="300px">M20</td>
                                                    <!--<td width="150px" data-id='td-litros'></td>-->
                                                    <td data-id='td-produtos'></td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="resultado" data-id='menor' style="display: none;">
                                            <div class="opcao-calc">Piscina Residencial de até 10 usuários</div>
                                            <p>
                                                Fazer a medição com o Mkit a cada 07 dias.
                                            </p>


                                            <table align="center" width="600px" data-id='transparente' style="display: none;">
                                                <tr>
                                                    <td width="300px">MPlus - Água transparente</td>
                                                    <!--<td width="150px" data-id='td-litros'></td>-->
                                                    <td data-id='td-produtos'>1000 ml</td>
                                                </tr>
                                            </table>

                                            <table align="center" width="600px" data-id='verde' style="display: none;">
                                                <tr>
                                                    <td width="300px">MPlus - Água verde</td>
                                                    <!--<td width="150px" data-id='td-litros'></td>-->
                                                    <td data-id='td-produtos'></td>
                                                </tr>
                                            </table>

                                            <table align="center" width="600px" data-id='m20'>
                                                <tr>
                                                    <td width="300px">M20</td>
                                                    <!--<td width="150px" data-id='td-litros'>10.000 litros</td>-->
                                                    <td data-id='td-produtos'>200 ml</td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="resultado" data-id='maior' style="display: none;">
                                            <div class="opcao-calc">Piscina Residencial com mais de 10 usuários</div>
                                            <p>
                                                Fazer a medição com o Mkit a cada 03 dias.
                                            </p>

                                            <table align="center" width="600px" data-id='transparente' style="display: none;">
                                                <tr>
                                                    <td width="300px">MPlus - Água transparente</td>
                                                    <!--<td width="150px" data-id='td-litros'></td>-->
                                                    <td data-id='td-produtos'>1000 ml</td>
                                                </tr>
                                            </table>

                                            <table align="center" width="600px" data-id='verde' style="display: none;">
                                                <tr>
                                                    <td width="300px">MPlus - Água verde</td>
                                                    <!--<td width="150px" data-id='td-litros'></td>-->
                                                    <td data-id='td-produtos'></td>
                                                </tr>
                                            </table>

                                            <table align="center" width="600px" data-id='m20'>
                                                <tr>
                                                    <td width="300px">M20</td>
                                                    <!--<td width="150px" data-id='td-litros'>10.000 litros</td>-->
                                                    <td data-id='td-produtos'>200 ml</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="btn proximo" data-id='calc3-show'>Avançar Calculadora</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-3">
                                <div class="col-md-12">
                                    <div class="title">Etapa 3 - Estética</div>
                                    <div class="col-md-12">
                                        <p>Consiste em adicionar à água clarificantes, decantadores e auxiliares de filtração, que farão com que toda a sujeira em suspensão se precipite, ou seja, retirada pelo filtro, eliminando a turbidez e fazendo com que a piscina fique cristalina. Veja a tabela abaixo para mais detalhes:</p>

                                        <div class="resultado" data-id='calc3-result'>
                                            <table align="center" width="600px">
                                                <tr>
                                                    <th width="150px">Produto</th>
                                                    <!--<th width="150px" class="text-center">Volume da Piscina</th>-->
                                                    <th width="150px" class="text-center">Problema</th>
                                                    <th width="150px" class="text-center">Dosagem</th>
                                                </tr>

                                                <tr data-id='mcfloc'>
                                                    <td>Mfloc</td>
                                                    <!--<td class="text-center" data-id='td-litros'>1.000</td>-->
                                                    <td class="text-center">Turbidez</td>
                                                    <td class="text-center" data-id='td-produtos'>4 ml</td>
                                                </tr>

                                                <tr data-id='clear_max'>
                                                    <td>Clear Max</td>
                                                    <!--<td class="text-center" data-id='td-litros'>1.000</td>-->
                                                    <td class="text-center">Turbidez Acentuada</td>
                                                    <td class="text-center" data-id='td-produtos'>5 ml</td>
                                                </tr>
                                                
<!--                                                <tr data-id='sequestrante-prevencao'>
                                                    <td>Sequestrante de Metais</td>
                                                    <td class="text-center" data-id='td-litros'>1.000</td>
                                                    <td class="text-center">Prevenção</td>
                                                    <td class="text-center" data-id='td-produtos'>15 ml</td>
                                                </tr>
                                                <tr data-id='sequestrande-manchas'>
                                                    <td>Sequestrante de Metais</td>
                                                    <td class="text-center" data-id='td-litros'>1.000</td>
                                                    <td class="text-center">Remoção de Manchas</td>
                                                    <td class="text-center" data-id='td-produtos'>50 ml</td>
                                                </tr>
                                                <tr data-id='algicida-choque'>
                                                    <td>Algicida de Choque</td>
                                                    <td class="text-center" data-id='td-litros'>1.000</td>
                                                    <td class="text-center">Água verde</td>
                                                    <td class="text-center" data-id='td-produtos'>15 ml</td>
                                                </tr>
                                                <tr data-id='algicida-manutencao'>
                                                    <td>Algicida de Manutenção</td>
                                                    <td class="text-center" data-id='td-litros'>1.000</td>
                                                    <td class="text-center">Prevenir água verde</td>
                                                    <td class="text-center" data-id='td-produtos'>5 ml</td>
                                                </tr>
                                                <tr data-id='eliminador-oleosidade'>
                                                    <td>Eliminador de Oleosidade</td>
                                                    <td class="text-center" data-id='td-litros'>1.000</td>
                                                    <td class="text-center">Gordura na água</td>
                                                    <td class="text-center" data-id='td-produtos'>7 ml</td>
                                                </tr>
                                                <tr data-id='eliminador-oleosidade2'>
                                                    <td>Eliminador de Oleosidade</td>
                                                    <td class="text-center" data-id='td-litros'>1.000</td>
                                                    <td class="text-center">Primeira dosagem</td>
                                                    <td class="text-center" data-id='td-produtos'>15 ml</td>
                                                </tr>
-->                                                <tr data-id='clear-gel'>
                                                    <td>Clear Gel</td>
                                                    <!--<td class="text-center" data-id='td-litros'>1.000</td>-->
                                                    <td class="text-center">Água levemente turva</td>
                                                    <td class="text-center" data-id='td-produtos'>1 gr</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1">

                                    </div>

                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="btn abre-video-3">Veja o vídeo explicativo</div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="btn proximo">Avançar Calculadora</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-3-video">
                                <div class="col-md-12">
                                    <div class="title">Vídeo explicativo</div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="videoWrapper">
                                            <!-- Copy & Pasted from YouTube -->
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/eKFTSSKCzWA?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-center">
                                        <div class="btn2 proximo">Voltar à Parte 3</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-4">
                                <div class="col-md-12">
                                    <div class="title">Etapa 4 - Física</div>
                                    <div class="col-md-12"><p>Consiste em utilizar materiais como aspiradores, esfregões, escovas e peneiras para retirar as sujeiras presentes na água, nas bordas, laterais e fundo.<br>É importante realizar os processos de aspiração para remover todas as partículas, além de remover as sujidades depositadas nas paredes e bordas.</p>
                                    <p>Quanto maior for o tempo de filtração diário, melhor o resultado final.</p></div>
                                </div>

                                <div class="col-md-12 text-center">
                                    <br>
                                    <div class="btn2 fecha-calc">Fechar Calculadora</div>
                                </div>
                            </div>

                        </div>

                        <div class="calculadoraspa text-left">
                            <div class="calc-1">
                                <div class="col-md-12">
                                    <div class="title">Etapa 1 - Sanitização</div>
                                    <div class="col-md-12"><p>A sanitização consiste em eliminar toda matéria orgânica e micro-organismos presentes na água, tornando-a própria para a utilização.</p></div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <li><input type="text" value="" data-id='spa_volume' class="form-control" placeholder="Volume da Água da SPA (litros)"></li>
                                        <br><br>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="opcao-calc">Estado da Água da SPA:</div>

                                        <input type="radio" name="estadoaguaspa" value="transparente" id="estadotransparentespa">
                                        <label for="estadotransparentespa"><span>Transparente</span></label>

                                        <input type="radio" name="estadoaguaspa" value="verde" id="estadoverdespa">
                                        <label for="estadoverdespa"><span>Verde</span></label>

                                        <div data-id="fraquencia-label">
                                            <div class="opcao-calc">Frequência de Usuários:</div>

                                            <input type="radio" name="frequenciausospa" value="diaria" id="spadiaria">
                                            <label for="spadiaria"><span>Diária</span></label>

                                            <input type="radio" name="frequenciausospa" value="semanal" id="spasemanal">
                                            <label for="spasemanal"><span>Semanal</span></label>
                                        </div>
                                    </div>

                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="btn abre-video">Veja o vídeo explicativo</div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="btn proximo" data-id='calc1-result-spa'>Ver resultado</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calcspa-video">
                                <div class="col-md-12">
                                    <div class="title">Vídeo explicativo</div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="videoWrapper">
                                            <!-- Copy & Pasted from YouTube -->
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/eKFTSSKCzWA?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-center">
                                        <div class="btn2 proximo">Voltar à Calculadora</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-1-result">
                                <div class="col-md-12">
                                    <div class="title">Etapa 1 - Sanitização</div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="resultado" data-id='diaria' style="display: none">
                                            <div class="opcao-calc">Uso diário</div>
                                            <p>
                                                Fazer a medição com o Kit Teste SPA a cada 03 dias.
                                            </p>
                                            <table align="center" width="600px">
                                                <tr>
                                                    <td width="150px">Sani SPA</td>
                                                    <!--<td width="150px" data-id='sani-litros'>1.000 litros</td>-->
                                                    <td width="150px">Sanitizar</td>
                                                    <td width="150px" data-id='sani-produtos'>300 ml</td>
                                                </tr>
                                                <tr>
                                                    <td>Oxi SPA</td>
                                                    <!--<td data-id='oxi-litros'>1.000 litros</td>-->
                                                    <td>Oxidar</td>
                                                    <td data-id='oxi-produtos'>300 ml</td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="resultado" data-id='semanal' style="display: none">
                                            <div class="opcao-calc">Uso semanal</div>
                                            <p>
                                                Fazer a medição com o Kit Teste SPA a cada 07 dias.
                                            </p>
                                            <table align="center" width="600px">
                                                <tr>
                                                    <td width="150px">Sani SPA</td>
                                                    <!--<td width="150px" data-id='sani-litros'>1.000 litros</td>-->
                                                    <td width="150px">Sanitizar</td>
                                                    <td width="150px" data-id='sani-produtos'>300 ml</td>
                                                </tr>
                                                <tr>
                                                    <td>Oxi SPA</td>
                                                    <!--<td data-id='oxi-litros'>1.000 litros</td>-->
                                                    <td>Oxidar</td>
                                                    <td data-id='oxi-produtos'>300 ml</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="btn proximo">Avançar Calculadora</div>
                                    </div>

                                </div>
                            </div>

                            <div class="calc-2">
                                <div class="col-md-12">
                                    <div class="title">Etapa 2 - Física</div>
                                    <div class="col-md-12"><p>Consiste em utilizar materiais como aspiradores, esfregões, escovas e peneiras para retirar as sujeiras presentes na água, nas bordas, laterais e fundo.<br>É importante realizar os processos de aspiração para remover todas as partículas, além de remover as sujidades depositadas nas paredes e bordas.</p>
                                    <p>Quanto maior for o tempo de filtração diário, melhor o resultado final.</p></div>
                                </div>

                                <div class="col-md-12 text-center">
                                    <br>
                                    <div class="btn2 fecha-calc">Fechar Calculadora</div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FECHA MODAL: Tratamento -->

        <!-- >> JS
        ============================================================================== -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/validate.js"></script>
        <!-- Roundabout -->
        <script src="vendor/jquery.roundabout.min.js"></script>
        <!-- Holder JS -->
        <script src="vendor/holder.js"></script>
        <!-- Fancybox -->
        <script src="vendor/fancybox/jquery.fancybox.js"></script>
        <!-- jQuery niceScroll -->
        <script src="vendor/jquery.nicescroll.min.js"></script>
        <script src="vendor/jquery.nicescroll.plus.js"></script>
        <!-- /jQuery niceScroll -->
        <!-- smoothScroll -->
        <script src="vendor/SmoothScroll.js"></script>
        <!-- Owl Caroulsel -->
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <!-- Cross-browser -->
        <script src="vendor/cross-browser.js"></script>
        <!-- Main Scripts -->
        <script src="js/main.js"></script>
        <script src="js/calculadora.js"></script>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="/vendor/html5shiv.js"></script>
          <script src="/vendor/respond.min.js"></script>
        <![endif]-->
        <!-- >> /JS
        ============================================================================= -->
    </body>
</html>
