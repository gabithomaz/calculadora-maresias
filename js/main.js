//Use Strict Mode
(function ($) {
    "use strict";

//Begin - Window Load
    $(window).load(function () {

        $('.calc1-ph, .calc1-alc').keyup(function(){
            var v = $(this).val();
            var v=v.replace(/\,/g, '.');
            var v = $(this).val(v);
        });

        $('.abre-calc2').click(function () {
            $('.body-tratamento').fadeOut(200);
            setTimeout(function () {
                $('.calculadoraspa .calc-1').fadeIn(300);
            }, 300);
        });

//        $('.calculadoraspa .calc-1 .proximo').click(function () {
//            $('.calculadoraspa .calc-1').fadeOut(200);
//            setTimeout(function () {
//                $('.calculadoraspa .calc-1-result').fadeIn(300);
//            }, 300);
//        });

        $('.calculadoraspa .calc-1-result .proximo').click(function () {
            $('.calculadoraspa .calc-1-result').fadeOut(200);
            setTimeout(function () {
                $('.calculadoraspa .calc-2').fadeIn(300);
            }, 300);
        });

        $('.calculadoraspa .fecha-calc').click(function () {
            $('input[type="text"]').val('');
            $("input:radio").attr("checked", false);
            $(".resultado").hide();
            $('.calculadoraspa .calc-2').fadeOut(200);
            setTimeout(function () {
                $('.body-tratamento').fadeIn(300);
            }, 300);
        });


        $('.abre-calc').click(function () {
            $('.body-tratamento').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-1').fadeIn(300);
            }, 300);
        });

//        $('.calculadora .calc-1 .proximo').click(function () {
//            $('.calculadora .calc-1').fadeOut(200);
//            setTimeout(function () {
//                $('.calculadora .calc-1-result').fadeIn(300);
//            }, 300);
//        });

        $('.calculadora .calc-1-result .proximo').click(function () {
            $('.calculadora .calc-1-result').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-2').fadeIn(300);
            }, 300);
        });

        $('.calculadora .abre-ph').click(function () {
            $('.calculadora .calc-1').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-1-ph').fadeIn(300);
            }, 300);
        });

        $('.calculadora .calc-1-ph .proximo').click(function () {
            $('.calculadora .calc-1-ph').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-1').fadeIn(300);
            }, 300);
        });

        $('.calculadora .abre-alcalinidade').click(function () {
            $('.calculadora .calc-1').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-1-alcalinidade').fadeIn(300);
            }, 300);
        });

        $('.calculadora .calc-1-alcalinidade .proximo').click(function () {
            $('.calculadora .calc-1-alcalinidade').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-1').fadeIn(300);
            }, 300);
        });

        $('.calculadora .abre-volume').click(function () {
            $('.calculadora .calc-1').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-1-volume').fadeIn(300);
            }, 300);
        });

        $('.calculadora .menu-piscinavolume-1').click(function () {
            $('.calculadora .piscinavolume').not('.piscinavolume-1').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .piscinavolume-1').fadeIn(300);
            }, 300);
        });

        $('.calculadora .menu-piscinavolume-2').click(function () {
            $('.calculadora .piscinavolume').not('.piscinavolume-2').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .piscinavolume-2').fadeIn(300);
            }, 300);
        });

        $('.calculadora .menu-piscinavolume-3').click(function () {
            $('.calculadora .piscinavolume').not('.piscinavolume-3').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .piscinavolume-3').fadeIn(300);
            }, 300);
        });

        $('.calculadora .menu-piscinavolume-4').click(function () {
            $('.calculadora .piscinavolume').not('.piscinavolume-4').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .piscinavolume-4').fadeIn(300);
            }, 300);
        });

        $('.calculadora .calc-1-volume .proximo').click(function () {
            $('.calculadora .calc-1-volume').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-1').fadeIn(300);
                volume = volume.replace(',', '.');
                volume = volume * 1000;
                volume = volume.toString();
                volume = volume.replace('.', ',');
                $('.calc1-volume').val(volume);
                $('.calc1-volume').focus();
            }, 300);
        });


//        $('.calculadora .calc-2 .proximo').click(function () {
//            $('.calculadora .calc-2').fadeOut(200);
//            setTimeout(function () {
//                $('.calculadora .calc-2-result').fadeIn(300);
//            }, 300);
//        });

//        $('.calculadora .calc-2-result .proximo').click(function () {
//            $('.calculadora .calc-2-result').fadeOut(200);
//            setTimeout(function () {
//                $('.calculadora .calc-3').fadeIn(300);
//            }, 300);
//        });

        $('.calculadora .calc-3 .proximo').click(function () {
            $('.calculadora .calc-3').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-4').fadeIn(300);
            }, 300);
        });

        $('.calculadora .fecha-calc').click(function () {
            $('input[type="text"]').val('');
            $("input:radio").attr("checked", false);
            $(".resultado").hide();
            $('.calculadora .calc-4').fadeOut(200);
            setTimeout(function () {
                $('.body-tratamento').fadeIn(300);
            }, 300);
        });

        $('.calculadora .abre-video-1').click(function () {
            $('.calculadora .calc-1').fadeOut(200);
            setTimeout(function () {
                $('.calc-1-video').fadeIn(300);
            }, 300);
        });

        $('.calculadora .calc-1-video .proximo').click(function () {
            $('.calculadora .calc-1-video').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-1').fadeIn(300);
            }, 300);
        });

        $('.calculadora .abre-video-2').click(function () {
            $('.calculadora .calc-2').fadeOut(200);
            setTimeout(function () {
                $('.calc-2-video').fadeIn(300);
            }, 300);
        });

        $('.calculadora .calc-2-video .proximo').click(function () {
            $('.calculadora .calc-2-video').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-2').fadeIn(300);
            }, 300);
        });

        $('.calculadora .abre-video-3').click(function () {
            $('.calculadora .calc-3').fadeOut(200);
            setTimeout(function () {
                $('.calc-3-video').fadeIn(300);
            }, 300);
        });

        $('.calculadora .calc-3-video .proximo').click(function () {
            $('.calculadora .calc-3-video').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-3').fadeIn(300);
            }, 300);
        });

        $('.calculadoraspa .abre-video').click(function () {
            $('.calculadoraspa .calc-1').fadeOut(200);
            setTimeout(function () {
                $('.calcspa-video').fadeIn(300);
            }, 300);
        });

        $('.calculadoraspa .calcspa-video .proximo').click(function () {
            $('.calculadoraspa .calcspa-video').fadeOut(200);
            setTimeout(function () {
                $('.calculadoraspa .calc-1').fadeIn(300);
            }, 300);
        });
//End - Document Ready
    });

//End - Use Strict mode
})(jQuery);
