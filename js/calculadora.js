var volume = 0;
$(document).ready(function () {
    //// MASK
    $('[data-id="input-volume"]').maskMoney({thousands: '.', allowZero: false, precision: 0});
    $('[data-id="spa_volume"]').maskMoney({thousands: '.', allowZero: false, precision: 0});

    ////// CALCULO DO VOLUME RETANGULAR
    function calcVolumeRetangular() {
        var comprimento = $(".retangular-comprimento").val();
        var largura = $(".retangular-largura").val();
        var profundidade = $(".retangular-profundidade").val();
        comprimento = comprimento.replace(',', '.');
        largura = largura.replace(',', '.');
        profundidade = profundidade.replace(',', '.');

        volume = comprimento * largura * profundidade;

        if (volume > 0) {
            volume = volume.toString();
            volume = volume.replace('.', ',');
            $('.calculadora .calc-1-volume .result-volume-retangular').text(volume);
        }
    }

    $(".input-retangular").change(function () {
        calcVolumeRetangular();
    });

    //////// CALCULO DE VOLUME REDONDA
    function calcVolumeRedonda() {
        var diametro = $('.redonda-diametro').val();
        var profundidade = $('.redonda-profundidade').val();
        diametro = diametro.replace(',', '.');
        profundidade = profundidade.replace(',', '.');

        volume = diametro * profundidade * 0.785;
        if (volume > 0) {
            volume = volume.toString();
            volume = volume.replace('.', ',');
            $('.result-volume-redonda').text(volume);
        }
    }


    $(".input-redonda").change(function () {
        calcVolumeRedonda();
    });

    //////// CALCULO DE VOLUME OITO
    function calcVolumeOito() {
        var maior = $('.oito-maior').val();
        var menor = $('.oito-menor').val();
        var profundidade = $('.oito-profundidade').val();
        maior = maior.replace(',', '.');
        menor = menor.replace(',', '.');
        profundidade = profundidade.replace(',', '.');

        volume = maior * menor * profundidade * 0.785;
        if (volume > 0) {
            volume = volume.toString();
            volume = volume.replace('.', ',');
            $('.result-volume-oito').text(volume);
        }

    }

    $(".input-oito").change(function () {
        calcVolumeOito();
    });

    $("[data-id='calc1-result']").click(function () {
        var volume = $('.calc1-volume').val();
        volume = volume.replace(/\./g, '');
        volume = volume / 1000;
        var ph = $('.calc1-ph').val();
        var alc = $('.calc1-alc').val();
        volume = volume.toString();
        ph = ph.replace(',', '.');
        alc = alc.replace(',', '.');
//      var litros = volume * 1000;
//      litros = litros.toString();

        if (ph !== '' && alc !== '' && volume !== '') {
            if (ph != '') {
                if (ph >= 7.2 && ph <= 7.6) {
                    $("[data-id='ph-ok']").fadeIn();
                } else if (ph < 7.2) {
                    $("[data-id='ph-baixo']").fadeIn();
                    if (ph < 6.8) {
                        quantidadeProdutoMin = volume * 15;
                        quantidadeProdutoMax = volume * 20;
                    } else if (ph >= 6.8 && ph <= 7) {
                        quantidadeProdutoMin = volume * 13;
                        quantidadeProdutoMax = volume * 16;
                    } else if (ph > 7 && ph <= 7.2) {
                        quantidadeProdutoMin = volume * 7;
                        quantidadeProdutoMax = volume * 13;
                    }

                    unidadeMedidaMin = 'ml';
                    unidadeMedidaMax = 'ml';

                    if (quantidadeProdutoMin >= 1000) {
                        unidadeMedidaMin = 'L';
                        quantidadeProdutoMin = quantidadeProdutoMin / 1000;
                    }

                    if (quantidadeProdutoMax >= 1000) {
                        unidadeMedidaMax = 'L';
                        quantidadeProdutoMax = quantidadeProdutoMax / 1000;
                    }

                    quantidadeProdutoMin = quantidadeProdutoMin.toString();
                    quantidadeProdutoMax = quantidadeProdutoMax.toString();
                    quantidadeProdutoMin = quantidadeProdutoMin.replace('.', ',');
                    quantidadeProdutoMax = quantidadeProdutoMax.replace('.', ',');
                    $("[data-id='ph-baixo']").find("[data-id='td-ph']").text(ph.replace('.', ','));
//               $("[data-id='ph-baixo']").find("[data-id='td-litros']").text(litros.replace('.', ',') + ' litros');


                    $("[data-id='ph-baixo']").find("[data-id='td-produtos']").text(quantidadeProdutoMin + unidadeMedidaMin + ' - ' + quantidadeProdutoMax + unidadeMedidaMax);

                } else if (ph > 7.6) {
                    if (ph >= 7.6 && ph <= 7.8) {
                        quantidadeProduto = volume * 3;
                    } else if (ph > 7.8 && ph <= 8) {
                        quantidadeProduto = volume * 5;
                    } else if (ph > 8) {
                        quantidadeProduto = volume * 10;
                    }

                    unidadeMedida = 'ml';
                    if (quantidadeProduto >= 1000) {
                        unidadeMedida = 'L';
                        quantidadeProduto = quantidadeProduto / 1000;
                    }

                    quantidadeProduto = quantidadeProduto.toString();
                    quantidadeProduto = quantidadeProduto.replace('.', ',');


                    $("[data-id='ph-alto']").find("[data-id='td-ph']").text(ph.replace('.', ','));
                    $("[data-id='ph-alto']").find("[data-id='td-produtos']").text(quantidadeProduto + unidadeMedida);
                    $("[data-id='ph-alto']").fadeIn();
                }
            }

            if (alc != '') {
                if (alc >= 80 && alc <= 120) {
                    $("[data-id='alc-ok']").fadeIn();
                } else if (alc < 80) {
                    $("[data-id='alc-baixa']").fadeIn();
                    quantidadeProduto = volume * 18;
                    
                    unidadeMedida = 'gr';
                    if (quantidadeProduto >= 1000) {
                        unidadeMedida = 'Kg';
                        quantidadeProduto = quantidadeProduto / 1000;
                    }
                    
                    quantidadeProduto = quantidadeProduto.toString();
                    quantidadeProduto = quantidadeProduto.replace('.', ',');
                    
                    $("[data-id='alc-baixa']").find("[data-id='td-ppm']").text(alc.replace('.', ',') + ' ppm');
                    $("[data-id='alc-baixa']").find("[data-id='td-produtos']").text(quantidadeProduto + unidadeMedida);
                } else if (alc > 120) {
                    $("[data-id='alc-alta']").fadeIn();
                    quantidadeProduto = volume * 10;
                    
                    unidadeMedida = 'ml';
                    if (quantidadeProduto >= 1000) {
                        unidadeMedida = 'L';
                        quantidadeProduto = quantidadeProduto / 1000;
                    }
                    
                    quantidadeProduto = quantidadeProduto.toString();
                    quantidadeProduto = quantidadeProduto.replace('.', ',');
                    
                    $("[data-id='alc-alta']").find("[data-id='td-alc']").text(alc.replace('.', ',') + ' ppm');
                    $("[data-id='alc-alta']").find("[data-id='td-produtos']").text(quantidadeProduto +  unidadeMedida);
                }
            }


            $('.calculadora .calc-1').fadeOut(200);
            setTimeout(function () {
                $('.calculadora .calc-1-result').fadeIn(300);
            }, 300);
        } else {
            $('.required').each(function () {
                if ($(this).val() == '') {
                    $(this).css('border', '2px solid #FF0000');
                }
            });
        }
    });

    $(".required").change(function () {
        if ($(this).val() == '') {
            $(this).css('border', '2px solid #FF0000');
        } else {
            $(this).css('border', '1px solid #02a1ce;');
        }
    });


    $("[data-id='calc2-result']").click(function () {
        var volume = $('.calc1-volume').val();
        volume = volume.replace(/\./g, '');
         volume = volume / 1000;
        var ph = $('.calc1-ph').val();
        var alc = $('.calc1-alc').val();
        var litros = volume * 1000;

        var estado_agua = $("input[name='estadoagua']:checked").val();
        var frequenciauso = $("input[name='frequenciauso']:checked").val();
        var tratamentoalternativo = $("input[name='tratamentoalternativo']:checked").val();

        $("[data-id='" + frequenciauso + "']").fadeIn();
//      $("[data-id='" + frequenciauso + "']").find("[data-id='" + estado_agua + "']").find("[data-id='td-litros']").text(litros + " litros");
        m20Produto = volume * 20;
        unidadeMedida = 'ml';
        if (m20Produto >= 1000) {
            unidadeMedida = 'L';
            m20Produto = m20Produto / 1000;
        }
        $("[data-id='" + frequenciauso + "']").find("[data-id='m20']").find("[data-id='td-produtos']").text(m20Produto + unidadeMedida);
//      $("[data-id='" + frequenciauso + "']").find("[data-id='m20']").find("[data-id='td-litros']").text(litros + " litros");
        litros = litros.toFixed(2);
        litros = litros.toString();
        litros = litros.replace('.', ',');


        if (frequenciauso == 'publica') {
            $("[data-id='" + frequenciauso + "']").find("[data-id='" + estado_agua + "']").fadeIn();
            if (estado_agua == 'transparente') {
                produto = volume * 100;
            } else if (estado_agua == 'verde') {
                produto = volume * 200;
            }
        } else if (frequenciauso == 'menor') {
            $("[data-id='" + frequenciauso + "']").find("[data-id='" + estado_agua + "']").fadeIn();
            if (estado_agua == 'transparente') {
                produto = volume * 100;
            } else if (estado_agua == 'verde') {
                produto = volume * 200;
            }
        } else if (frequenciauso == 'maior') {
            $("[data-id='" + frequenciauso + "']").find("[data-id='" + estado_agua + "']").fadeIn();
            if (estado_agua == 'transparente') {
                produto = volume * 100;
            } else if (estado_agua == 'verde') {
                produto = volume * 200;
            }
        }

        unidadeMedida = 'ml';
        if (produto >= 1000) {
            unidadeMedida = 'L';
            produto = produto / 1000;
        }
        
//        produto = produto.toFixed(2);
        produto = produto.toString();
        produto = produto.replace('.', ',');


//        $("[data-id='" + frequenciauso + "']").find("[data-id='" + estado_agua + "']").find("[data-id='td-litros']").text(litros + " litros");
        $("[data-id='" + frequenciauso + "']").find("[data-id='" + estado_agua + "']").find("[data-id='td-produtos']").text(produto + unidadeMedida);
        $('.calculadora .calc-2').fadeOut(200);
        setTimeout(function () {
            $('.calculadora .calc-2-result').fadeIn(300);
        }, 300);
    });

    function removeDot(number) {
        number = number.toString();
        number = number.replace('.', ',');

        return number;
    }

    $("[data-id='calc3-show']").click(function () {
        var volume = $('.calc1-volume').val();
        volume = volume.replace(/\./g, '');
        volume = volume / 1000;

        var ph = $('.calc1-ph').val();
        var alc = $('.calc1-alc').val();
        var litros = volume * 1000;
        litros = litros.toFixed(2);

        var mcfloc = volume * 4;
        mcfloc = removeDot(mcfloc);
        var clear_max = volume * 5;
        clear_max = removeDot(clear_max);
        var sequestrante_prevencao = volume * 15;
        sequestrante_prevencao = removeDot(sequestrante_prevencao);
        var sequestrante_manchas = volume * 50;
        sequestrante_manchas = removeDot(sequestrante_manchas);
        var algicida_choque = volume * 15;
        algicida_choque = removeDot(algicida_choque);
        var algicida_manutencao = volume * 5;
        algicida_manutencao = removeDot(algicida_manutencao);
        var eliminador_oleosidade = volume * 7;
        eliminador_oleosidade = removeDot(eliminador_oleosidade);
        var eliminador_oleosidade2 = volume * 15;
        eliminador_oleosidade2 = removeDot(eliminador_oleosidade2);
        var clear_gel = volume * 1;
        clear_gel = removeDot(clear_gel);
        
        unidadeMcFlooc = 'ml';
        if(mcfloc >= 1000){
            unidadeMcFlooc = 'L';
            mcfloc = mcfloc / 1000;
        }
        
        unidadeClearMax = 'ml';
        if(clear_max >= 1000){
            unidadeClearMax = 'L';
            clear_max = mcfloc / 1000;
        }


//      $("[data-id='calc3-result']").find("[data-id='td-litros']").text(volume + "m³");

        $("[data-id='calc3-result']").find("[data-id='mcfloc']").find("[data-id='td-produtos']").text(mcfloc + unidadeMcFlooc);
        $("[data-id='calc3-result']").find("[data-id='clear_max']").find("[data-id='td-produtos']").text(clear_max + unidadeClearMax);
//      $("[data-id='calc3-result']").find("[data-id='sequestrante-prevencao']").find("[data-id='td-produtos']").text(sequestrante_prevencao + " ml");
//      $("[data-id='calc3-result']").find("[data-id='sequestrante-manchas']").find("[data-id='td-produtos']").text(sequestrante_prevencao  + " ml");
//      $("[data-id='calc3-result']").find("[data-id='algicida-choque']").find("[data-id='td-produtos']").text(algicida_choque + " ml");
//      $("[data-id='calc3-result']").find("[data-id='algicida-manutencao']").find("[data-id='td-produtos']").text(algicida_manutencao + " ml");
//      $("[data-id='calc3-result']").find("[data-id='eliminador-oleosidade']").find("[data-id='td-produtos']").text(eliminador_oleosidade + " ml");
//      $("[data-id='calc3-result']").find("[data-id='eliminador-oleosidade2']").find("[data-id='td-produtos']").text(eliminador_oleosidade2 + " ml");
//      $("[data-id='calc3-result']").find("[data-id='clear-gel']").find("[data-id='td-produtos']").text(clear_gel + " gr");



        $('.calculadora .calc-2-result').fadeOut(200);
        setTimeout(function () {
            $('.calculadora .calc-3').fadeIn(300);
        }, 300);
    });



    ////// SPA'S

    $("[data-id='calc1-result-spa']").click(function () {
        var volume_spa = $("[data-id='spa_volume']").val();
        volume = volume.replace(/\./g, '');
        volume = volume / 1000;
        volume_spa = volume_spa.replace(',', '.');
        var estado_agua_spa = $("input[name='estadoaguaspa']:checked").val();
        var frequenciauso_spa = $("input[name='frequenciausospa']:checked").val();
//        produto = (volume_spa / 1000) * 300;
        produto = volume_spa * 300;

        unidadeMedida = 'ml';
        if(produto >= 1000){
            unidadeMedida = 'L';
            produto = produto / 1000; 
            produto = produto.toFixed(2);
            produto = produto.replace('.', ',');
        }


        volume_spa = volume_spa.replace('.', ',');
        

        if (volume_spa != '' && (frequenciauso_spa == 'diaria' || frequenciauso_spa == 'semanal')) {
//            $("[data-id='" + frequenciauso_spa + "']").find("[data-id='sani-litros']").text(volume_spa + ' litros');
//            $("[data-id='" + frequenciauso_spa + "']").find("[data-id='oxi-litros']").text(volume_spa + ' litros');
            $("[data-id='" + frequenciauso_spa + "']").find("[data-id='sani-produtos']").text(produto + unidadeMedida);
            $("[data-id='" + frequenciauso_spa + "']").find("[data-id='oxi-produtos']").text(produto + unidadeMedida);

            $("[data-id='" + frequenciauso_spa + "']").fadeIn();

            $('.calculadoraspa .calc-1').fadeOut(200);
            setTimeout(function () {
                $('.calculadoraspa .calc-1-result').fadeIn(300);
            }, 300);
        } else {
            if ($("[data-id='spa_volume']").val() == '') {
                $("[data-id='spa_volume']").css('border', '2px solid #FF0000');
            } else {
                $("[data-id='spa_volume']").css('border', '1px solid #02a1ce;');
            }

            $('input[name="frequenciausospa"]').addClass('input-error');
        }
    });

    $("[data-id='spa_volume']").change(function () {
        if ($(this).val() == '') {
            $(this).css('border', '2px solid #FF0000');
        } else {
            $(this).css('border', '1px solid #02a1ce;');
        }
    });

    $('input[name="frequenciausospa"]').click(function () {
        $('input[name="frequenciausospa"]').removeClass('input-error');
    });

});
